CREATE TABLE item (
	item_id int NOT NULL AUTO_INCREMENT primary key,
	item_name VARCHAR(256) NOT NULL,
	item_price int NOT NULL,
	category_id int
	);
